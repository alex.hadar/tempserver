package util// Copyright 2000-2021 JetBrains s.r.o. and contributors. Use of this source code is governed by the Apache 2.0 license that can be found in the LICENSE file.
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.WindowSize
import androidx.compose.ui.window.WindowState
import androidx.compose.ui.window.application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.http.HttpStatusCode
import io.ktor.http.Parameters
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.serialization.json
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import java.util.Date

fun main() {
    var results: List<TempData> by mutableStateOf(listOf())

    // Aici "pornim" severul: il punem sa asculte pe portul

    embeddedServer(Netty, port = 80) {
        install(ContentNegotiation) {
            json(kotlinx.serialization.json.Json {
                ignoreUnknownKeys = true
            })
        }

        // routing se refera unde asteptam sa ne vina rezultatele.
        // In cazul acesta, dupa ip-ul si portul selectat, asteptam pe /data?temp=...
        // e.g. http://127.0.0.1:80/data?temp=20.08

        routing {
            // metoda GET pe /data
            get("/data") {
                // Luam din URL parametrul nostru "temp" (poti sa il intrebi si pe vladut cum functioneaza
                // un url cu query param
                val queryParams: Parameters = call.request.queryParameters
                if (!queryParams.isEmpty()) {
                    val data = queryParams["temp"]?.toFloatOrNull()
                    if (data != null) {
                        // Adaugam la lista noastra obiectul cu timestamp-ul de acum
                        results = results + TempData(data, Date(System.currentTimeMillis()))
                    }
                }

                // Zicem ca totul e ok
                call.respond(HttpStatusCode.OK)
            }
        }
    }.start()

    /*
     * Aici deschidem noi ecranul acela si asteptam rezultatele.
     *
     */
    application {
        Window(
            title = "TEMP RECORDER - Stoicoiu Laura",
            onCloseRequest = ::exitApplication,
            state = WindowState(size = WindowSize(1024.dp, 800.dp))
        ) {
            AppScreen(results)
        }
    }
}
