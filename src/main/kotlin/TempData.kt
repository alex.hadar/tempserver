package util

import java.util.Date

data class TempData(
    val temp: Float,
    val timestamp: Date,
)
