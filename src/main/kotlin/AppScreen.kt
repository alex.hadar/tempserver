package util

import androidx.compose.desktop.DesktopMaterialTheme
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import java.text.SimpleDateFormat

val formatter: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")

// Aici cream UI-ul propriu zis.

@Composable
fun AppScreen(items: List<TempData>) = DesktopMaterialTheme {
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        // in acest "Column" pune bara aia rosie de sus

        Column(
            modifier = Modifier.fillMaxWidth().size(64.dp).background(Color.Red).padding(4.dp),
            horizontalAlignment = Alignment.Start,
        ) {
            Text(text = "Sample size: ${items.size}", color = Color.White, fontSize = 16.sp)
            Spacer(modifier = Modifier.size(8.dp))
            if (items.isNotEmpty()) {
                val average = ((items.sumOf { it.temp.toDouble() } / items.size) * 100).toInt() / 100f
                Text(text = "Average temp: $average °C", color = Color.White, fontSize = 30.sp)
            }
        }
        ResultsList(modifier = Modifier.fillMaxSize(), items = items)
    }
}

/**
 * Asta contine lista noastra de rezultate.
 * Pur si simplu le afisam pe rand, de la primul la ultimul
 */

@Composable
private fun ResultsList(
    modifier: Modifier = Modifier,
    items: List<TempData>,
) {
    val state: LazyListState = rememberLazyListState()

    LazyColumn(
        modifier = modifier,
        state = state,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        itemsIndexed(items) { index, result ->
            ResultCard(index, result)
            ResultCardDivider()
        }
    }
    if (items.isNotEmpty()) {
        LaunchedEffect(items.size) {
            state.scrollToItem(items.lastIndex)
        }
    }
}

@Composable
private fun ResultCardDivider() = Divider(
    modifier = Modifier.fillMaxWidth(),
    thickness = 1.dp,
    color = Color.LightGray,
    startIndent = 4.dp
)

/**
 * Aici cream o linie cu un rezultat.
 */

@Composable
private fun ResultCard(
    index: Int,
    item: TempData,
) {
    val indexString = when {
        index < 10 -> "00$index"
        index < 100 -> "0$index"
        else -> index.toString()
    }
    val backgroundColor = if (index % 2 == 0) Color.LightGray.copy(alpha = 0.20f) else Color.White
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .height(64.dp)
            .background(backgroundColor)
            .padding(horizontal = 16.dp),
        verticalArrangement = Arrangement.SpaceEvenly,
    ) {
        Text("Timetamp: ${formatter.format(item.timestamp)}")
        Text("$indexString | ${item.temp} °C", fontSize = 24.sp, textAlign = TextAlign.Center)
    }
}